import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {BaconComponent} from './bacon.component';

const routes: Routes = [{
  path: '',
  title: 'Bacon | Dr.Clockworks\' Cabinet of Curiosities',
  component: BaconComponent,
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BaconRoutingModule {
}
