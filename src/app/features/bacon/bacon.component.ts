import {AfterViewInit, Component, ElementRef, ViewChild} from '@angular/core';
import * as Matter from 'matter-js';

@Component({
  selector: 'app-bacon',
  templateUrl: './bacon.component.html',
  styleUrls: ['./bacon.component.scss']
})
export class BaconComponent implements AfterViewInit {

  @ViewChild('world') world: ElementRef;
  engine: Matter.Engine;
  mouse: Matter.Mouse;

  pan: Matter.Composite;
  handBacon: Matter.Composite;
  target: Matter.Body;
  targetRender: Matter.Body;
  handBaconConstraint: Matter.Constraint;
  mainScale: number;
  panPosition: { x: number, y: number };
  loading = true;

  svgRoots: { [key: string]: Document } = {};
  dragCategory = 0x0001;
  noDragCategory = 0x0002;
  targetIndex = 0;
  targetNames = [
    'on the plate!',
    'on the cat!',
    'on the foot!',
    'on the tuba!',
    'on the hanger!',
  ];
  targets: Matter.Body[];
  targetsRender: Matter.Body[];

  constructor() {
  }

  ngAfterViewInit(): void {
    // Load SVGs
    this.loadSvgs().then(() => {
      this.runEngine();
    });
  }

  private runEngine() {
    // create an engine
    this.engine = Matter.Engine.create({
      // enableSleeping: true,
    });

    this.engine.gravity.y = 0.6;

    // create a renderer
    const render = Matter.Render.create({
      element: this.world.nativeElement,
      engine: this.engine,
      options: {
        width: this.world.nativeElement.offsetWidth,
        height: this.world.nativeElement.offsetHeight - 10,
        wireframes: false,
        background: '#925462',
      }
    });

    // add mouse control
    this.mouse = Matter.Mouse.create(render.canvas);
    const mouseConstraint = Matter.MouseConstraint.create(this.engine, {
      mouse: this.mouse,
      collisionFilter: {
        // @ts-ignore
        mask: this.dragCategory,
      },
      /* @ts-ignore */
      constraint: {
        stiffness: 0.2,
        render: {
          visible: false
        }
      }
    });

    this.setMouseEvents(mouseConstraint);

    Matter.Composite.add(this.engine.world, mouseConstraint);

    // run the renderer
    Matter.Render.run(render);
    // create runner
    const runner = Matter.Runner.create();
    // run the engine
    Matter.Runner.run(runner, this.engine);

    this.addBounds();
    this.setBaconCollisionEvent();
    this.addPan();
    this.loading = false;
    this.addTarget();
    this.addBacon();
  }

  private addPan() {
    let panWidth = Math.max(this.world.nativeElement.offsetHeight, this.world.nativeElement.offsetHeight) / 6;
    if (panWidth < 200) {
      panWidth = 200;
    }
    this.mainScale = panWidth;
    const panHeight = 0.1 * panWidth;
    const panSlope = .2;

    this.panPosition = {
      x: 0.7 * panWidth,
      y: this.world.nativeElement.offsetHeight / 2,
    };

    const pan = Matter.Bodies.trapezoid(
      this.panPosition.x,
      this.panPosition.y,
      panWidth,
      panHeight,
      panSlope,
      {
        frictionAir: 0,
        // @ts-ignore
        chamfer: 5,
        mass: 20,
        render: {
          fillStyle: '#454545',
        }
      }
    );

    const handle = Matter.Bodies.rectangle(
      this.panPosition.x + (0.7 * panWidth),
      this.panPosition.y + (0.8 * panHeight),
      (0.7 * panWidth),
      (0.5 * panHeight),
      {
        frictionAir: 0,
        // @ts-ignore
        chamfer: 5,
        render: {
          fillStyle: '#a06e2b',
        }
      }
    );

    Matter.Body.rotate(handle, 0.2);

    const fullPan = Matter.Body.create({parts: [pan, handle], collisionFilter: {category: this.noDragCategory}});

    Matter.Body.rotate(fullPan, 3.5);

    this.pan = Matter.Composite.create({label: 'Pan Composite'});
    Matter.Composite.add(this.pan, fullPan);

    // Tilt constraint
    Matter.Composite.add(this.pan, Matter.Constraint.create({
      bodyB: fullPan,
      pointB: {x: -0.3 * panWidth, y: 1.1 * panHeight},
      pointA: {x: fullPan.position.x - (0.3 * panWidth), y: fullPan.position.y - (1.1 * panHeight)},
      stiffness: 0.9,
      length: 0,
      render: {
        visible: false,
      }
    }));

    // Reset constraint
    const panConstraint = Matter.Constraint.create({
      bodyB: fullPan,
      pointB: {x: 0.5 * panWidth, y: (2 * panHeight)},
      pointA: {x: fullPan.position.x + 0.4 * panWidth, y: fullPan.position.y + (4 * panHeight)},
      stiffness: 0.0001,
      length: 2 * panHeight,
      render: {
        visible: false,
      }
    });
    Matter.Composite.add(this.pan, panConstraint);
    Matter.Composite.add(this.engine.world, this.pan);

    const panGround = Matter.Bodies.rectangle(
      fullPan.position.x,
      fullPan.position.y + (0.4 * panWidth),
      panWidth,
      panHeight * 2,
      {
        isStatic: true,
        render: {
          visible: false,
        }
      }
    );

    Matter.Composite.add(this.engine.world, [panGround]);

    // Defines targets
    const vertexSetsCat = this.select(this.svgRoots.cat, 'path')
      .map(path => {
        // @ts-ignore
        return Matter.Vertices.scale(Matter.Svg.pathToVertices(path, 30), -0.4, 0.4);
      });

    const vertexSetsFoot = this.select(this.svgRoots.foot, 'path')
      .map(path => {
        // @ts-ignore
        return Matter.Vertices.scale(Matter.Svg.pathToVertices(path, 30), -0.4, 0.4);
      });

    const vertexSetsTuba = this.select(this.svgRoots.tuba, 'path')
      .map(path => {
        // @ts-ignore
        return Matter.Vertices.scale(Matter.Svg.pathToVertices(path, 30), -0.4, 0.4);
      });

    const vertexSetsHanger = this.select(this.svgRoots.hanger, 'path')
      .map(path => {
        // @ts-ignore
        return Matter.Vertices.scale(Matter.Svg.pathToVertices(path, 30), -0.05, -0.05);
      });

    this.targets = [
      Matter.Bodies.trapezoid(
        6 * this.panPosition.x,
        this.panPosition.y,
        300,
        30,
        -0.3,
        {
          isStatic: true,
          label: 'target',
          // @ts-ignore
          chamfer: 5,
          render: {
            fillStyle: '#fae2d0',
          },
        }
      ),
      Matter.Bodies.fromVertices(6 * this.panPosition.x, this.panPosition.y, vertexSetsCat, {
        isStatic: true,
        label: 'target',
        render: {
          visible: false,
        }
      }, true),
      Matter.Bodies.fromVertices(6 * this.panPosition.x, this.panPosition.y, vertexSetsFoot, {
        isStatic: true,
        label: 'target',
        render: {
          visible: false,
        }
      }, true),
      Matter.Bodies.fromVertices(6 * this.panPosition.x, 0.8 * this.panPosition.y, vertexSetsTuba, {
        isStatic: true,
        label: 'target',
        render: {
          visible: false,
        }
      }, true),
      Matter.Bodies.fromVertices(6 * this.panPosition.x, this.panPosition.y, vertexSetsHanger, {
        isStatic: true,
        label: 'target',
        friction: 0,
        frictionStatic: 0,
        render: {
          lineWidth: 4,
          strokeStyle: 'black',
        }
      }, true),
    ];

    this.targetsRender = [
      null,
      Matter.Bodies.rectangle(
        this.targets[1].position.x + 10,
        this.targets[1].position.y - 10,
        10,
        10,
        {
          isStatic: true,
          render: {
            sprite: {
              texture: '/assets/bacon/cat.svg',
              xScale: 1.4,
              yScale: 1.4,
            }
          }
        }
      ),
      Matter.Bodies.rectangle(
        this.targets[2].position.x - 10,
        this.targets[2].position.y - 40,
        10,
        10,
        {
          isStatic: true,
          render: {
            sprite: {
              texture: '/assets/bacon/foot.svg',
              xScale: 1.4,
              yScale: 1.4,
            }
          }
        }
      ),
      Matter.Bodies.rectangle(
        this.targets[3].position.x - 15,
        this.targets[3].position.y + 60,
        10,
        10,
        {
          isStatic: true,
          render: {
            sprite: {
              texture: '/assets/bacon/tuba.svg',
              xScale: 1.4,
              yScale: 1.4,
            }
          }
        }
      ),
      null,
    ];
  }

  private addBacon() {
    if (this.handBacon) {
      Matter.Composite.remove(this.engine.world, this.handBacon);
      this.handBacon = null;
    }
    const columns = 2;
    const rows = 8;
    const columnGap = this.mainScale * 0.02;
    const rowGap = this.mainScale * 0.02;
    const radius = this.mainScale * 0.02;

    const particleOptions: Matter.IBodyDefinition = {
      collisionFilter: {
        category: this.noDragCategory,
      },
      inertia: Infinity,
      frictionAir: 0.0001,
      mass: 0.2,
      friction: .3,
      label: 'bacon',
    };
    const constraintOptions = {
      stiffness: .2,
      render: {
        fillStyle: '#b27695',
        strokeStyle: '#b27695',
        lineWidth: 1,
      }
    };

    // Hand SVG
    this.handBacon = Matter.Composite.create({label: 'Hand Bacon Composite'});
    const color = Matter.Common.choose(['#f19648', '#ded196', '#ca9c93']);

    const vertexSets = this.select(this.svgRoots.hand, 'path')
      .map(path => {
        // @ts-ignore
        return Matter.Vertices.scale(Matter.Svg.pathToVertices(path, 30), -0.4, 0.4);
      });

    const hand = Matter.Bodies.fromVertices(this.panPosition.x, 0.3 * this.panPosition.y, vertexSets, {
      isStatic: true,
      render: {
        fillStyle: color,
        lineWidth: 1,
        strokeStyle: color,
      }
    }, true);

    Matter.Composite.add(this.handBacon, hand);

    let colorI = 0;
    const colors = [
      '#cc9eb3',
      '#89486b',
    ];
    const bacon = Matter.Composites.stack(
      hand.position.x,
      0.5 * hand.position.y + 150,
      columns,
      rows,
      columnGap,
      rowGap,
      (x, y) => {
        const baconColor = colors[colorI];
        colorI++;
        if (colorI > 1) {
          colorI = 0;
        }
        return Matter.Bodies.circle(x, y, radius, {
          ...particleOptions,
          render: {
            fillStyle: baconColor,
            strokeStyle: baconColor,
            lineWidth: 10,
          }
        });
      }
    );

    Matter.Composites.mesh(bacon, columns, rows, true, constraintOptions);
    Matter.Composite.add(this.handBacon, bacon);

    // Hand/Bacon constraint
    this.handBaconConstraint = Matter.Constraint.create({
      bodyB: bacon.bodies[0],
      pointA: {x: hand.position.x + 10, y: hand.position.y + 50},
      stiffness: 1,
      length: 0,
      render: {
        visible: false,
      }
    });
    Matter.Composite.add(this.handBacon, this.handBaconConstraint);
    Matter.Composite.add(this.engine.world, this.handBacon);
    Matter.Body.setVelocity(bacon.bodies[bacon.bodies.length - 1], {x: 40, y: 0});
  }

  private tiltPan() {
    Matter.Body.applyForce(this.pan.bodies[0], {
      x: this.pan.bodies[0].position.x,
      y: this.pan.bodies[0].position.y
    }, {x: 0, y: -1});
  }

  private setBaconCollisionEvent() {
    const timeouts = [];
    Matter.Events.on(this.engine, 'collisionStart', (event) => {
      const pairs = event.pairs;
      for (const pair of pairs) {
        // Out of bounds
        if (pair.bodyA.label !== pair.bodyB.label
          && (
            (pair.bodyA.label === 'bacon' && pair.bodyB.label === 'bound')
            || (pair.bodyA.label === 'bound' && pair.bodyB.label === 'bacon')
          )
        ) {
          this.addBacon();
          break;
        }

        // Target reached
        if (pair.bodyA.label !== pair.bodyB.label
          && (
            (pair.bodyA.label === 'bacon' && pair.bodyB.label === 'target')
            || (pair.bodyA.label === 'target' && pair.bodyB.label === 'bacon')
          )
        ) {
          timeouts.push(setTimeout(() => {
            const baconPair = pair.bodyA.label === 'bacon' ? pair.bodyA : pair.bodyB;
            if (Math.abs(baconPair.velocity.x) < 1 && Math.abs(baconPair.velocity.y) < 1) {
              for (const timeout of timeouts) {
                clearTimeout(timeout);
              }
              this.success();
            }
          }, 2000));
          break;
        }
      }

    });
  }

  private addBounds() {
    // Add bounds
    Matter.Composite.add(this.engine.world, [
      Matter.Bodies.rectangle(
        this.world.nativeElement.offsetWidth / 2 + 200,
        -200, this.world.nativeElement.offsetWidth,
        50,
        {
          isStatic: true,
          label: 'bound'
        }
      ), // Top
      Matter.Bodies.rectangle(
        this.world.nativeElement.offsetWidth / 2 + 200,
        this.world.nativeElement.offsetHeight + 200,
        this.world.nativeElement.offsetWidth,
        50,
        {
          isStatic: true,
          label: 'bound'
        }
      ), // Bottom
      Matter.Bodies.rectangle(
        -200,
        this.world.nativeElement.offsetHeight / 2 + 200,
        50,
        this.world.nativeElement.offsetHeight,
        {
          isStatic: true,
          label: 'bound'
        }
      ), // Left
      Matter.Bodies.rectangle(
        this.world.nativeElement.offsetWidth + 200,
        this.world.nativeElement.offsetHeight / 2 + 200,
        50,
        this.world.nativeElement.offsetHeight,
        {
          isStatic: true,
          label: 'bound'
        }
      ) // Right
    ]);
  }

  private addTarget() {
    if (this.target) {
      Matter.World.remove(this.engine.world, this.target);
      if (this.targetRender) {
        Matter.World.remove(this.engine.world, this.targetRender);
        this.targetRender = null;
      }
      this.target = null;
      this.targetIndex++;
      if (this.targetIndex >= this.targets.length) {
        this.targetIndex = 0;
      }
    }
    this.target = this.targets[this.targetIndex];
    this.targetRender = this.targetsRender[this.targetIndex];
    Matter.Composite.add(this.engine.world, this.target);
    if (this.targetRender) {
      Matter.Composite.add(this.engine.world, this.targetRender);
    }
  }

  private success() {
    Matter.Composite.remove(this.engine.world, this.handBacon);
    this.addBacon();
    this.addTarget();
  }

  private loadSvgs() {
    const svgs = {
      hand: '/assets/bacon/hand.svg',
      cat: '/assets/bacon/cat-blank.svg',
      foot: '/assets/bacon/foot-blank.svg',
      tuba: '/assets/bacon/tuba-blank.svg',
      hanger: '/assets/bacon/hanger.svg',
    };

    const promises = [];

    for (const key in svgs) {
      if (svgs[key]) {
        promises.push(fetch(svgs[key])
          .then(response => response.text())
          .then(raw => {
            this.svgRoots[key] = (new window.DOMParser()).parseFromString(raw, 'image/svg+xml');
          }));
      }
    }

    return Promise.all(promises);
  }

  private select(root, selector) {
    return Array.prototype.slice.call(root.querySelectorAll(selector));
  }

  private setMouseEvents(mouseConstraint: Matter.MouseConstraint) {
    Matter.Events.on(mouseConstraint, 'mousedown', (event) => {
      if (!mouseConstraint.body) {
        if (this.handBaconConstraint && this.handBacon) {
          Matter.Composite.remove(this.handBacon, this.handBaconConstraint);
          this.handBaconConstraint = null;
        } else {
          this.tiltPan();
        }
      }
    });
  }
}
