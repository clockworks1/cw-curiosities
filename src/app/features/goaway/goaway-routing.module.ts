import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {GoawayComponent} from './goaway.component';

const routes: Routes = [{
  path: '',
  title: 'GO AWAY | Dr.Clockworks\' Cabinet of Curiosities',
  component: GoawayComponent,
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GoawayRoutingModule {
}
