import {Component, ElementRef, HostListener, OnInit, QueryList, ViewChild, ViewChildren} from '@angular/core';

@Component({
  selector: 'app-goaway',
  templateUrl: './goaway.component.html',
  styleUrls: ['./goaway.component.scss'],
})
export class GoawayComponent implements OnInit {

  audio: any;
  audioPlaying = false;
  private gain: GainNode;

  constructor() {
  }

  @ViewChildren('eyes') eyes: QueryList<ElementRef>;
  @ViewChildren('eyebrows') eyebrows: QueryList<ElementRef>;
  @ViewChild('mouth') mouth: ElementRef;

  private initEyeBrowRotate = -40;

  ngOnInit(): void {
    this.audio = new Audio();
    this.audio.load();
  }

  @HostListener('mousemove', ['$event']) onMouseMove(event) {
    if (this.eyes.length) {
      for (const element of this.eyes) {
        const eye = element.nativeElement;
        let mouseX = eye.getBoundingClientRect().right;
        if (eye.classList.contains('eye-left')) {
          mouseX = eye.getBoundingClientRect().left;
        }
        const mouseY = eye.getBoundingClientRect().top;
        const radianDegrees = Math.atan2(event.pageX - mouseX, event.pageY - mouseY);
        const rotationDegrees = radianDegrees * (180 / Math.PI) * -1 + 180;
        eye.style.transform = `rotate(${rotationDegrees}deg)`;
      }
    }

    if (this.mouth) {
      const middleX = window.innerWidth / 2;
      const middleY = window.innerHeight / 2;

      const distanceHeadPointer = this.getDistance(event.x, event.y, middleX, middleY);
      const distanceHeadFurthest = this.getDistance(middleX, middleY, window.innerWidth, window.innerHeight);

      let closeness = (distanceHeadFurthest - distanceHeadPointer) / distanceHeadFurthest;

      if (!this.audioPlaying) {
        const audioContext = new window.AudioContext();
        audioContext.resume();
        this.gain = audioContext.createGain();
        this.gain.connect(audioContext.destination);
        fetch('../../../assets/goaway/goaway.mp3').then((resp) => {

          resp.arrayBuffer().then((buffer) => {
            audioContext.decodeAudioData(buffer, (abuffer) => {
              const track = audioContext.createBufferSource();
              track.buffer = abuffer;
              track.connect(this.gain);
              track.loop = true;
              track.start();
            });
          });
        });
        this.audioPlaying = true;
      }

      this.gain.gain.value = closeness;

      if (closeness < 0.3) {
        closeness = 0;
      }

      const opening = closeness * 35;
      const mouth = this.mouth.nativeElement;

      mouth.style.transform = `translateY(${opening}px)`;

      if (this.eyebrows.length) {
        for (const element of this.eyebrows) {
          const eyebrow = element.nativeElement;
          let eyebrowRotate = closeness * 80 + this.initEyeBrowRotate;
          if (eyebrow.classList.contains('eyebrow-right')) {
            eyebrowRotate = (closeness * 80 + this.initEyeBrowRotate) * -1;
          }
          eyebrow.style.transform = `rotate(${eyebrowRotate}deg)`;
        }
      }
    }

  }

  private getDistance(x1, y1, x2, y2) {
    const y = x2 - x1;
    const x = y2 - y1;

    return Math.sqrt(x * x + y * y);
  }

}
