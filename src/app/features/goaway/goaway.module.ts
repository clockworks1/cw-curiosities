import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GoawayRoutingModule } from './goaway-routing.module';
import { GoawayComponent } from './goaway.component';


@NgModule({
  declarations: [
    GoawayComponent
  ],
  imports: [
    CommonModule,
    GoawayRoutingModule
  ]
})
export class GoawayModule { }
