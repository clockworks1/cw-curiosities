import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private router: Router) {
  }

  ngOnInit(): void {
  }

  showCuriosity() {
    const routes = this.router.config.filter(route => route.path && route.path !== '**');
    const random = Math.random();
    const randomIndex = Math.floor(random * routes.length);
    const randomRoute = routes[randomIndex];
    window.open(randomRoute.path);
  }
}
