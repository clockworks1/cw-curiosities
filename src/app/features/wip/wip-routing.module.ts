import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {WipComponent} from './wip.component';

const routes: Routes = [{
  path: '',
  title: 'WIP | Dr.Clockworks\' Cabinet of Curiosities',
  component: WipComponent,
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WipRoutingModule {
}
