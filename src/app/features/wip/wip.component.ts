import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-wip',
  templateUrl: './wip.component.html',
  styleUrls: ['./wip.component.scss']
})
export class WipComponent implements OnInit {

  progress = '';

  constructor() {
  }

  ngOnInit(): void {
    this.addProgress();
  }

  private addProgress() {
    setTimeout(() => {
      const random = Math.round(Math.random() * 5);
      for (let i = 0; i < random; i++) {
        this.progress += 'X';
      }
      this.addProgress();
    }, 1000);
  }

}
