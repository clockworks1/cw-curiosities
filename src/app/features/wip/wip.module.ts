import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WipRoutingModule } from './wip-routing.module';
import { WipComponent } from './wip.component';


@NgModule({
  declarations: [
    WipComponent
  ],
  imports: [
    CommonModule,
    WipRoutingModule
  ]
})
export class WipModule { }
