import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {JoydiComponent} from './joydi.component';

const routes: Routes = [{
  path: '',
  title: 'New disorder | Dr.Clockworks\' Cabinet of Curiosities',
  component: JoydiComponent,
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JoydiRoutingModule {
}
