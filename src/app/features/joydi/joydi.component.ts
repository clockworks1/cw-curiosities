import {AfterViewInit, Component, ElementRef, HostListener, ViewChild} from '@angular/core';

@Component({
  selector: 'app-joydi',
  templateUrl: './joydi.component.html',
  styleUrls: ['./joydi.component.scss']
})
export class JoydiComponent implements AfterViewInit {

  @ViewChild('joydi') joydi: ElementRef;
  step = 10;
  lines = [];
  canvas;
  canvasContext: CanvasRenderingContext2D;

  pointerPos: { x, y };

  constructor() {
  }

  ngAfterViewInit(): void {
    this.canvas = this.joydi.nativeElement;
    this.canvasContext = this.canvas.getContext('2d');


    let innerSize = window.innerWidth;
    if (innerSize > window.innerHeight) {
      innerSize = window.innerHeight;
    }
    const size = innerSize;
    const dpr = window.devicePixelRatio;
    this.canvas.width = size * dpr;
    this.canvas.height = size * dpr;
    this.canvasContext.scale(dpr, dpr);
    this.canvasContext.lineWidth = 2;
    // Create the lines
    const lines = [];
    for (let i = this.step; i <= size - this.step; i += this.step) {
      const line = [];
      for (let j = this.step; j <= size - this.step; j += this.step) {
        const distanceToCenter = Math.abs(j - size / 2);
        const variance = Math.max(size / 6 - distanceToCenter, 0);
        const random = Math.random() * variance / 3 * -1;

        const point = {x: j, y: i + random};
        line.push(point);
      }
      lines.push(line);
    }
    this.lines = lines;

    this.draw(size);
  }

  @HostListener('mousemove', ['$event']) onMouseMove(event) {
    const boundingCanvas = this.canvas.getBoundingClientRect();
    this.pointerPos = {x: event.clientX - boundingCanvas.left, y: event.clientY - boundingCanvas.top};
  }

  private draw(size) {
    this.canvasContext.clearRect(0, 0, this.canvas.width, this.canvas.height);

    for (const line of this.lines) {
      for (const point of line) {

        const distanceToCenter = Math.abs(point.x - size / 2);
        const variance = Math.max(size / 6 - distanceToCenter, 0);
        const varianceEffect = variance ? (-1 * variance / 100) : 0;

        if (!point.shift) {
          point.shift = Math.round(Math.random()) ? 1 : -1;
          point.shiftDirection = point.shift;
        }

        if (point.shiftDirection === 1) {
          point.shift++;
        } else if (point.shiftDirection === -1) {
          point.shift--;
        }
        if (point.shift >= (-1 * varianceEffect * 3)) {
          point.shift = -1;
          point.shiftDirection = -1;
        } else if (point.shift <= (varianceEffect * 3)) {
          point.shift = 1;
          point.shiftDirection = 1;
        }

        point.y = point.y + (varianceEffect * point.shift);
      }
    }

    // Do the drawing
    for (let i = 5; i < this.lines.length; i++) {
      this.canvasContext.beginPath();
      this.canvasContext.moveTo(this.lines[i][0].x, this.lines[i][0].y);

      for (let j = 0; j < this.lines[i].length - 2; j++) {
        let distanceVariance = 0;
        if (this.pointerPos) {
          distanceVariance = this.getDistanceVariance(this.lines[i][j].x, this.lines[i][j].y, this.pointerPos.x, this.pointerPos.y);
        }
        const xc = (this.lines[i][j].x + this.lines[i][j + 1].x) / 2;
        const yc = ((this.lines[i][j].y + this.lines[i][j + 1].y) / 2) + distanceVariance;
        this.canvasContext.quadraticCurveTo(this.lines[i][j].x, this.lines[i][j].y + distanceVariance, xc, yc);
      }

      this.canvasContext.quadraticCurveTo(
        this.lines[i][this.lines[i].length - 2].x,
        this.lines[i][this.lines[i].length - 2].y,
        this.lines[i][this.lines[i].length - 1].x,
        this.lines[i][this.lines[i].length - 1].y
      );
      this.canvasContext.save();
      this.canvasContext.globalCompositeOperation = 'destination-out';
      this.canvasContext.fill();
      this.canvasContext.restore();
      this.canvasContext.stroke();
    }


    setTimeout(() => {
      this.draw(size);
    }, 10);
  }

  private getDistanceVariance(x1, y1, x2, y2) {
    const y = x2 - x1;
    const x = y2 - y1;

    const distance = Math.sqrt(x * x + y * y);
    return -1 / distance * 200;
  }
}
