import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { JoydiRoutingModule } from './joydi-routing.module';
import { JoydiComponent } from './joydi.component';


@NgModule({
  declarations: [
    JoydiComponent
  ],
  imports: [
    CommonModule,
    JoydiRoutingModule
  ]
})
export class JoydiModule { }
