import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MinuscortexRoutingModule } from './minuscortex-routing.module';
import { MinuscortexComponent } from './minuscortex.component';
import {SharedModule} from '../../shared/shared.module';


@NgModule({
  declarations: [
    MinuscortexComponent
  ],
  imports: [
    CommonModule,
    MinuscortexRoutingModule,
    SharedModule
  ]
})
export class MinuscortexModule { }
