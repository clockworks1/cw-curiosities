import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MinuscortexComponent} from './minuscortex.component';

const routes: Routes = [{
  path: '',
  title: 'C\'EST MINUS ET CORTEX ET MINUS | Dr.Clockworks\' Cabinet of Curiosities',
  component: MinuscortexComponent,
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MinuscortexRoutingModule {
}
