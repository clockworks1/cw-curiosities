import {AfterViewInit, Component, ElementRef, ViewChild} from '@angular/core';

@Component({
  selector: 'app-minuscortex',
  templateUrl: './minuscortex.component.html',
  styleUrls: ['./minuscortex.component.scss']
})
export class MinuscortexComponent implements AfterViewInit {

  go: boolean;
  @ViewChild('minus') minus: ElementRef;
  @ViewChild('cortex') cortex: ElementRef;
  angleMinus = -45;
  angleCortex = 45;
  angleSwitch = true;
  scale = 1;
  maxScale = 100;

  constructor() {
  }

  ngAfterViewInit(): void {
    this.growHeads();
  }

  private growHeads() {
    if (this.go) {
      if (this.angleSwitch) {
        this.angleMinus += 90;
        this.angleCortex -= 90;
      } else {
        this.angleMinus -= 90;
        this.angleCortex += 90;
      }
      if (this.scale < 10) {
        this.scale = this.scale + .1;
      }
      this.angleSwitch = !this.angleSwitch;
      this.minus.nativeElement.style.transform = `rotate(${this.angleMinus}deg) scale(${this.scale})`;
      this.cortex.nativeElement.style.transform = `rotate(${this.angleCortex}deg) scale(${this.scale})`;
    }
    setTimeout(() => {
      this.growHeads();
    }, 500);

  }

}
