import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WordartRoutingModule } from './wordart-routing.module';
import { WordartComponent } from './wordart.component';
import {SharedModule} from '../../shared/shared.module';
import {FormsModule} from '@angular/forms';


@NgModule({
  declarations: [
    WordartComponent
  ],
  imports: [
    CommonModule,
    WordartRoutingModule,
    SharedModule,
    FormsModule
  ]
})
export class WordartModule { }
