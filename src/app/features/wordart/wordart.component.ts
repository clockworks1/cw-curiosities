import {AfterViewChecked, AfterViewInit, Component, ElementRef, ViewChild} from '@angular/core';

@Component({
  selector: 'app-wordart',
  templateUrl: './wordart.component.html',
  styleUrls: ['./wordart.component.scss']
})
export class WordartComponent implements AfterViewInit, AfterViewChecked {

  text: string;
  @ViewChild('input') input: ElementRef;

  classes = [
    'outline', 'up', 'arc', 'squeeze', 'inverted-arc', 'basic-stack',
    'italic-outline', 'slate', 'mauve', 'graydient', 'red-blue', 'brown-stack',
    'radial', 'purple', 'green-marble', 'rainbow', 'aqua', 'texture-stack',
    'paper-bag', 'sunset', 'tilt', 'blues', 'yellow-dash', 'green-stack',
    'chrome', 'marble-slab', 'gray-block', 'superhero', 'horizon', 'stack-3d'
  ];
  backgrounds = [
    'bkgd01', 'bkgd02', 'bkgd03', 'bkgd04', 'bkgd05', 'bkgd06', 'bkgd07', 'bkgd08', 'bkgd09', 'bkgd10',
    'bkgd11', 'bkgd12', 'bkgd13', 'bkgd14', 'bkgd15', 'bkgd16', 'bkgd17', 'bkgd18', 'bkgd19', 'bkgd20',
    'bkgd21', 'bkgd22', 'bkgd23', 'bkgd24', 'bkgd25', 'bkgd26', 'bkgd27', 'bkgd28', 'bkgd29', 'bkgd30',
  ];

  selectedClass: string;
  selectedBackground: string;

  constructor() {
  }

  ngAfterViewInit() {
    this.updateClasses();
  }

  ngAfterViewChecked() {
    this.input.nativeElement.focus();
  }

  updateClasses() {
    const randomClass = Math.random();
    const randomClassIndex = Math.floor(randomClass * this.classes.length);
    this.selectedClass = this.classes[randomClassIndex];
    const randomBackground = Math.random();
    const randomBackgroundIndex = Math.floor(randomBackground * this.backgrounds.length);
    this.selectedBackground = this.backgrounds[randomBackgroundIndex];
  }

  refocus() {
    this.input.nativeElement.focus();
  }
}
