import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {WordartComponent} from './wordart.component';

const routes: Routes = [{
  path: '',
  title: 'I love Wordart | Dr.Clockworks\' Cabinet of Curiosities',
  component: WordartComponent,
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WordartRoutingModule {
}
