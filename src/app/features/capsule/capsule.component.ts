import {AfterViewInit, Component, ElementRef, ViewChild} from '@angular/core';
import * as Matter from 'matter-js';

@Component({
  selector: 'app-capsule',
  templateUrl: './capsule.component.html',
  styleUrls: ['./capsule.component.scss']
})
export class CapsuleComponent implements AfterViewInit {

  @ViewChild('world') world: ElementRef;
  engine: Matter.Engine;
  mouse: Matter.Mouse;

  capsules = [
    {
      link: 'capsule1.png',
      width: 300,
      height: 218,
    },
    {
      link: 'capsule2.png',
      width: 172,
      height: 300,
    },
    {
      link: 'capsule3.png',
      width: 293,
      height: 300,
    },
    {
      link: 'capsule4.png',
      width: 186,
      height: 300,
    },
    {
      link: 'capsule5.png',
      width: 182,
      height: 300,
    },
  ];

  constructor() {
  }

  ngAfterViewInit(): void {
    this.runEngine();
  }

  private runEngine() {
    // create an engine
    this.engine = Matter.Engine.create();

    console.log(this.world.nativeElement);
    // create a renderer
    const render = Matter.Render.create({
      element: this.world.nativeElement,
      engine: this.engine,
      options: {
        width: this.world.nativeElement.offsetWidth,
        height: this.world.nativeElement.offsetHeight - 10,
        wireframes: false,
        background: '#012345',
      }
    });

    const ground = Matter.Bodies.rectangle(
      this.world.nativeElement.offsetWidth / 2,
      this.world.nativeElement.offsetHeight,
      this.world.nativeElement.offsetWidth,
      10,
      {isStatic: true}
    );
    const wallLeft = Matter.Bodies.rectangle(
      0,
      this.world.nativeElement.offsetHeight / 2,
      1,
      this.world.nativeElement.offsetHeight,
      {isStatic: true}
    );
    const wallRight = Matter.Bodies.rectangle(
      this.world.nativeElement.offsetWidth,
      this.world.nativeElement.offsetHeight / 2,
      1,
      this.world.nativeElement.offsetHeight,
      {isStatic: true}
    );

    // add all of the bodies to the world
    Matter.Composite.add(this.engine.world, [ground, wallLeft, wallRight]);

    // add mouse control
    this.mouse = Matter.Mouse.create(render.canvas);
    const mouseConstraint = Matter.MouseConstraint.create(this.engine, {
      mouse: this.mouse,
      /* @ts-ignore */
      constraint: {
        stiffness: 0.2,
        render: {
          visible: false
        }
      }
    });

    Matter.Events.on(mouseConstraint, 'mousedown', (event) => {
      if (!mouseConstraint.body) {
        this.addNewCapsule();
      }
    });

    Matter.Composite.add(this.engine.world, mouseConstraint);

    // run the renderer
    Matter.Render.run(render);
    // create runner
    const runner = Matter.Runner.create();
    // run the engine
    Matter.Runner.run(runner, this.engine);

    this.addNewCapsule(true);
  }

  private addNewCapsule(first?: boolean) {
    const random = Math.random();
    const randomIndex = Math.floor(random * this.capsules.length);
    const randomCapsule = this.capsules[randomIndex];
    const newCapsule = Matter.Bodies.rectangle(
      first ? this.world.nativeElement.offsetWidth / 2 : this.mouse.position.x,
      first ? -200 : this.mouse.position.y,
      randomCapsule.width - 100,
      randomCapsule.height - 100,
      {
        restitution: 1,
        render: {
          sprite: {
            texture: `assets/capsule/${randomCapsule.link}`,
            xScale: 1,
            yScale: 1,
          }
        }
      }
    );
    // add all of the bodies to the world
    Matter.Composite.add(this.engine.world, newCapsule);
  }

}
