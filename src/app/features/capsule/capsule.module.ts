import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CapsuleRoutingModule } from './capsule-routing.module';
import { CapsuleComponent } from './capsule.component';


@NgModule({
  declarations: [
    CapsuleComponent
  ],
  imports: [
    CommonModule,
    CapsuleRoutingModule
  ]
})
export class CapsuleModule { }
