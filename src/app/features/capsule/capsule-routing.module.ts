import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {CapsuleComponent} from './capsule.component';

const routes: Routes = [{
  path: '',
  title: 'Capsule | Dr.Clockworks\' Cabinet of Curiosities',
  component: CapsuleComponent,
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CapsuleRoutingModule { }
