import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FlipflopRoutingModule } from './flipflop-routing.module';
import { FlipflopComponent } from './flipflop.component';
import {SharedModule} from '../../shared/shared.module';


@NgModule({
  declarations: [
    FlipflopComponent
  ],
    imports: [
        CommonModule,
        FlipflopRoutingModule,
        SharedModule
    ]
})
export class FlipflopModule { }
