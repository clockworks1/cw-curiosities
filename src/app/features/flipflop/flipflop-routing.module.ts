import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {FlipflopComponent} from './flipflop.component';

const routes: Routes = [{
  path: '',
  title: 'Which flip flop are you? | Dr.Clockworks\' Cabinet of Curiosities',
  component: FlipflopComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FlipflopRoutingModule { }
