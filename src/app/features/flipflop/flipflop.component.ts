import {Component} from '@angular/core';

@Component({
  selector: 'app-flipflop',
  templateUrl: './flipflop.component.html',
  styleUrls: ['./flipflop.component.scss']
})
export class FlipflopComponent {

  yourFlipflop: { title: string, description: string, link: string; };

  flipflops = [
    {
      link: '01',
      title: 'You\'re the classic flip flop!',
      description: 'You have no personality, do you?',
    },
    {
      link: '02',
      title: 'You\'re the toilet paper flip flop !',
      description: 'It may be useful.',
    },
    {
      link: '03',
      title: 'You\'re the sandwich flip flop!',
      description: 'Go on, take a bite!',
    },
    {
      link: '04',
      title: 'You\'re the grass flip flop!',
      description: 'Mother nature would be proud!',
    },
    {
      link: '05',
      title: 'You\'re the fish flop !',
      description: 'Bloop.',
    },
    {
      link: '06',
      title: 'You\'re the rainbow flip flop!',
      description: 'Colors! Colors! I see colors!',
    },
    {
      link: '07',
      title: 'You\'re the sandal!',
      description: 'What\'s wrong with you?',
    },
    {
      link: '08',
      title: 'You\'re the kitty flip flop!',
      description: 'Aaaaaaaaw so cuuuuuuute!!!!! *vomits*',
    },
    {
      link: '09',
      title: 'You\'re the inflatable flip flop!',
      description: 'The funniest person at the pool!',
    },
    {
      link: '10',
      title: 'You\'re the giant flip flop!',
      description: 'Not very practical but impressive!',
    },
    {
      link: '11',
      title: 'You\'re the japanese flip flop!',
      description: 'インターネットで「トン」を翻訳しました',
    },
    {
      link: '12',
      title: 'You\'re a duck!',
      description: 'Quack!',
    },
  ];

  looping = false;

  constructor() {
  }

  launchLoop() {
    if (!this.looping) {
      this.looping = true;
      this.loop();
      setTimeout(() => {
        this.looping = false;
      }, 1000);
    }
  }

  private loop() {
    if (this.looping) {
      const random = Math.random();
      const randomIndex = Math.floor(random * this.flipflops.length);
      this.yourFlipflop = this.flipflops[randomIndex];
    }
    setTimeout(() => {
      this.loop();
    }, 50);
  }
}
