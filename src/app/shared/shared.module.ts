import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {MatCardModule} from '@angular/material/card';
import {MatListModule} from '@angular/material/list';
import {ExamplePipe} from './pipes/example.pipe';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatButtonModule} from '@angular/material/button';
import {DataTextDirective} from './directives/data-text.directive';
import {MatInputModule} from '@angular/material/input';

const MATERIAL_MODULES = [
  MatListModule,
  MatCardModule,
  MatSnackBarModule,
  MatButtonModule,
  MatInputModule,
];

@NgModule({
  declarations: [
    ExamplePipe,
    DataTextDirective,
  ],
  exports: [
    ...MATERIAL_MODULES,
    ExamplePipe,
    DataTextDirective,
  ],
  imports: [
    CommonModule,
    ...MATERIAL_MODULES,
  ],
})

export class SharedModule {
}
