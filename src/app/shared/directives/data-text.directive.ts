import {Directive, ElementRef, Input, OnChanges, SimpleChanges} from '@angular/core';

@Directive({
  selector: '[appDataText]'
})
export class DataTextDirective implements OnChanges {
  @Input() appDataText;

  constructor(private el: ElementRef) {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.appDataText) {
      this.el.nativeElement.setAttribute('data-text', this.appDataText || '');
    }
  }
}
