import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';


const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./features/home/home.module').then(mod => mod.HomeModule),
  },
  {
    path: 'ilovewordart',
    loadChildren: () => import('./features/wordart/wordart.module').then(mod => mod.WordartModule),
  },
  {
    path: 'joydi',
    loadChildren: () => import('./features/joydi/joydi.module').then(mod => mod.JoydiModule),
  },
  {
    path: 'goaway',
    loadChildren: () => import('./features/goaway/goaway.module').then(mod => mod.GoawayModule),
  },
  {
    path: 'flipflop',
    loadChildren: () => import('./features/flipflop/flipflop.module').then(mod => mod.FlipflopModule),
  },
  {
    path: 'capsule',
    loadChildren: () => import('./features/capsule/capsule.module').then(mod => mod.CapsuleModule),
  },
  {
    path: 'minuscortex',
    loadChildren: () => import('./features/minuscortex/minuscortex.module').then(mod => mod.MinuscortexModule),
  },
  {
    path: 'bacon',
    loadChildren: () => import('./features/bacon/bacon.module').then(mod => mod.BaconModule),
  },
  {
    path: 'wip',
    loadChildren: () => import('./features/wip/wip.module').then(mod => mod.WipModule),
  },
  {
    path: '**',
    redirectTo: '',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
