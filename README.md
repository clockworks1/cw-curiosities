# Dr. ClockWorks's Cabinet of Curiosities

Inspired by Tom Holman's The Useless Web.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.1.3 and updated to 14.

## Installation

Create a .env file in the root directory from the .env.dist file and edit the ports if you need to (4200 and 9876 work out of the box).

## Development server

Run `docker-compose up` to run a dev server in a container. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Get inside your container by running `docker-compose exec web bash`.

Run `npm run ng -- generate component component-name` to generate a new component. `component-name` can also be path.
Use the `--dry-run` option to simulate your command.

You can also use `npm run ng generate -- directive|pipe|service|class|guard|interface|enum|module`.

More info on best practices below.

## Build

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory. Use `npm build:prod` for a production build.

## Running unit tests

Run `npm run test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Project architecture

While Angular does not really provide best practices about the project architecture, we came up with this one based on Angular's documentation. It works and has been used in many projects of different scales.

### What you need to know

The `src/app` folder contains the base `app` component and three directories:

**Core** should contain two directories: `api` and `models`. The api directory will contain base services to make requests to your backend or external APIs. The models directory will contain TS interfaces for those API's responses.

**Features** should contain modules according to your needs. A module should be an independent element of your application, for example a profile page or a todo list.

**Shared** should contain components and services that are reusable across your application. The main SharedService might be useful for some utilities like a loading bar management or interactions with your local storage for example.

The **Shared** directory will also contain your pipes, directives, guards or modules, anything that might be used anywhere.

### Module generation

Use these commands to generate a new module and its base component:

    npm run ng -- generate module --routing features/your-module
    npm run ng -- generate component features/your-module
    npm run ng -- generate service features/your-module

### Code style and best practices

- Please avoid using the `any` type, take the time to create classes and interfaces that match your object.
  - Raw responses should match interfaces defined in `core/models`
  - Custom object should be defined in `features/your-feature/models` or `shared/models`
- Do not hesitate to create modules and submodules, it really helps readability in the end.
- A new module should be generated with the `--routing` flag to separate routes from module definition.
- Each module should have its own service.
- Please avoid using Promises, Observables are really nice!
- Private functions should be defined at the end of your components
- Variables should be camelCase
- **DO NOT** use the html `style` attribute, scss files are waaaaay better.
- Angular Material imports should be located in the SharedModule, this package provides many useful components but not all of them are imported at once. If you need one, chances are the rest of the app will use it.
